let M = {}, V = {}, C = {};

// Model - process data

let name = document.getElementById('name').value;
let address = document.getElementById('address').value;
let email = document.getElementById('email').value;
let phone = document.getElementById('phone').value;  
let website = document.getElementById('website').value;

M = {
  data: {
    name: name.value,
    address: address.value,
    email: email.value,
    phone: phone.value,
    website: website.value
  },
  setData: function(d){
    this.data.name = d.name;
    this.data.address = d.address;
    this.data.email = d.email;
    this.data.phone = d.phone;
    this.data.website = d.website;
  },
  getData: function(){
    return data;
  }
}


// View - display data

V = {
  name: name.value,
  address: address.value,
  email: email.value,
  phone: phone.value,
  website: website.value,

  update: function createObject(M) {
  
    // create a newUser object 
    let newUser = {
      'name': name,
      'address': address,
      'email': email,
      'phone': phone,
      'website': website
    };
  
    let str = JSON.stringify(newUser);
    localStorage.setItem("User Info", str)
  }
}

// Controller - receive user input
C = {
  model: M,
  view: V,
  handler: function(){
      this.view.update(this.model);
  }
}

document.getElementById('submit').addEventListener('click', function () {
  C.handler.call(C);
})