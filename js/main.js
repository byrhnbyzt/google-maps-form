'use strict';

// form validation
function validateForm() {
  const name = document.inputForm.name;
  const address = document.inputForm.address;
  const email = document.inputForm.email;
  const phone = document.inputForm.phone;  
  const website = document.inputForm.website;

  function validateEmail(email) {
    const regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(email.value);
  }

  // Check if name is filled
  if (name.value == "") {
    alert("Please enter your name.");
    return false;
  }

  if (address.value == "") {
    alert("Please enter your address.");
    return false;
  } 

  if (email.value == "") {
    alert("Please enter a valid e-mail address.");
    return false;
  }

  if (!validateEmail(email)) {
    alert("Please enter a valid e-mail address.")
    return false;
  }

  if (phone.value == "") {
    alert("Please enter a phone number.");
    return false;
  }
  
  if (isNaN(phone.value)) {
    alert("Please enter a phone number."); 
    return false;
  } 
  
  if (website.value == "") {
    alert("Please enter your website.");
    return false;
  }

  return true;
}

// create object and save it to localStorage
function createObject() {
  const name = document.inputForm.name.value;
  const address = document.inputForm.address.value;
  const email = document.inputForm.email.value;
  const phone = document.inputForm.phone.value;  
  const website = document.inputForm.website.value;

  // create a newUser object 
  let newUser = {
    'name': name,
    'address': address,
    'email': email,
    'phone': phone,
    'website': website
  };

  let str = JSON.stringify(newUser);
  localStorage.setItem("User Info", str)
}

// google maps

let map;
let geocoder;
let marker = "";

// Initialize map
function initMap() {
  geocoder = new google.maps.Geocoder;
  let input = document.getElementById('address');
  let autocomplete = new google.maps.places.Autocomplete(input);
  let mapOptions = {
    center: {lat: 42.706659563510, lng:23.318481445313},
    zoom: 8
  }
  map = new google.maps.Map(document.getElementById('map'), mapOptions);
}

// Convert address to lat/lng
function locationConvert() {  
  let address = document.getElementById('address').value;
  geocoder.geocode({'address': address}, function(results, status) {
    if (status == 'OK') {
      map.setCenter(results[0].geometry.location);
      if(marker) {
        marker.setMap(null) 
      }
      marker = new google.maps.Marker({
        map: map,
        position: results[0].geometry.location
      });
    } else {
      alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}

// Find location manually - onclick get lat/lng and convert it to readable address
function getLocation() {
  google.maps.event.addListener(map, 'click', function(event) {
    // get lat/lon of click
    let getLat = event.latLng.lat();
    let getLng = event.latLng.lng();

    // Put a marker to clicked place
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(getLat, getLng),
      map: map
    });

    // Reverse geocoding convert from lat/lng to readable address
    let latLng = {lat: getLat, lng: getLng};
    geocoder.geocode({'location': latLng}, function(results, status) {
      if (status === 'OK') {
        if (results[0]) {    
          document.getElementById('address').value = (results[0].formatted_address);
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }
    });
  });
}



// Event listener for submit button
document.getElementById('submit').addEventListener('click', function(){
  validateForm();
  createObject();
  locationConvert();
});

// Event listener for "Find location manually" button
document.getElementById('findLoc').addEventListener('click', function(){
  getLocation();
});